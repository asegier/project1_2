window.utils = {
  el,
  requestAnimFrame,
  calcDistance,
  calcBazookaDamage,
}

function el (id) {
  return document.getElementById(id)
}

function requestAnimFrame (callback) {
  if (window.requestAnimationFrame) {
    window.requestAnimationFrame(callback)
    return
  }
  if (window.webkitRequestAnimationFrame) {
    window.webkitRequestAnimationFrame(callback)
    return
  }
  if (window.mozRequestAnimationFrame) {
    window.mozRequestAnimationFrame(callback)
    return
  }
  setTimeout(callback, 1000 / 60)
}

function calcDistance (playerX, rocketX, playerY, rocketY) {
  // pythagoras triangle equation
  // var xDistance = playerX - rocketX
  // var yDistnace = playerY - rocketY
  // var c = Math.sqrt( a*a + b*b );
  
  let distance = (playerX - rocketX)**2 + (playerY - rocketY)**2
  // convert to absolute distance in case we have a negative number
  // we are only interested in absolute value here
  if (distance < 0) {
    distance = distance * -1
  }
  return Math.sqrt(distance)
}

function calcBazookaDamage (collisionDistance) {
  const maxDamage = 30
  return 30 - collisionDistance
}
